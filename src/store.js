import { createStore, combineReducers } from 'redux';
import { recipeDataBase,menuDatabase, slidingPanelsControl } from './components/reducers';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';



const reducers = {
    recipeDataBase,
    menuDatabase, 
    slidingPanelsControl
}

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2,
} 

const rootReducers = combineReducers(reducers);
const persistedReducers = persistReducer(persistConfig, rootReducers);

export const configureStore = () => createStore(persistedReducers,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);