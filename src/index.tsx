import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router } from 'react-router-dom';
import { configureStore} from './store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react'
import { persistStore } from 'redux-persist';
import ScrollToTop from './components/ScrollToTop';

const store = configureStore();
const persistor = persistStore(store);


ReactDOM.render(
    <Provider store={store}>
        <PersistGate 
            loading={<div>Loading...</div>}
            persistor={persistor}>
            <Router>
                <ScrollToTop />
                <App />
            </Router>
        </PersistGate>
    </Provider>, 
    document.getElementById('root')
);


serviceWorker.unregister();

