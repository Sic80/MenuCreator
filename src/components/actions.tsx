import { Recipe } from './types/RecipeInterface';
import RecipeDatabaseActionsTypes from './types/RecipeDatabaseActionsTypes';
import MenuDatabaseActionsTypes from './types/MenuDatabaseActionsTypes';
import SlidingPanelsControlActionsTypes from './types/SlidingPanelsControlActionsTypes';
import { Table } from './types/TableInterface';

const createAction = <T,>(type: string, payload?: T) => ({ type, payload });


export const SAVE_RECIPE = "SAVE_RECIPE";
export const saveRecipe = (recipe: Recipe) => createAction(RecipeDatabaseActionsTypes.SAVE_RECIPE, {recipe});

export const DELETE_RECIPE = "DELETE_RECIPE";
export const deleteRecipe = (idx: number) => createAction(RecipeDatabaseActionsTypes.DELETE_RECIPE, { idx });



export const SAVE_MENU = "SAVE_MENU";
export const saveMenu = (date: string, tableValues: Table[]) => createAction(MenuDatabaseActionsTypes.SAVE_MENU, { date, tableValues });

export const DELETE_MENU = "DELETE_MENU";
export const deleteMenu = (menuIdx: number) => createAction(MenuDatabaseActionsTypes.DELETE_MENU, { menuIdx });

export const SAVE_MENU_TO_SEND = "SAVE_MENU_TO_SEND";
export const saveMenuToSend = (menu: Table[] | {}) => createAction(MenuDatabaseActionsTypes.SAVE_MENU_TO_SEND, { menu });



export const TOGGLE_RECIPES_PANELS = "TOGGLE_RECIPES_PANELS";
export const toggleRecipesPanels = (idx1: number, idx2?: number) => createAction(SlidingPanelsControlActionsTypes.TOGGLE_RECIPES_PANELS, { idx1, idx2 });

export const TOGGLE_OLD_MENUS_PANELS = "TOGGLE_OLD_MENUS_PANELS";
export const toggleOldMenusPanels = (idx1: number, idx2?: number) => createAction(SlidingPanelsControlActionsTypes.TOGGLE_OLD_MENUS_PANELS, { idx1, idx2 });

export const TOGGLE_SHOPPING_LIST_PANEL = "TOGGLE_SHOPPING_LIST_PANEL";
export const toggleShoppingListPanel = () => createAction(SlidingPanelsControlActionsTypes.TOGGLE_SHOPPING_LIST_PANEL);

export const TOGGLE_EDIT_RECIPE_PANEL = "TOGGLE_EDIT_RECIPE_PANEL";
export const toggleEditRecipePanel = () => createAction(SlidingPanelsControlActionsTypes.TOGGLE_EDIT_RECIPE_PANEL);