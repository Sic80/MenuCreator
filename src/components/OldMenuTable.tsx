import { Table } from './types/TableInterface';

interface OldMenuTableProps {
    oldMenuTable: Table[],
    tableWidth: string
}

const OldMenuTable = ({ oldMenuTable, tableWidth }: OldMenuTableProps) => {

    return ( 
        <table className={tableWidth}> 
            <tbody>
                <tr>
                    {["Lunch", "Dinner"].map((name, idx) => <th key={idx} className="oldmenu_table-th">{name}</th>)}
                </tr>                                                               
                {oldMenuTable &&
                    oldMenuTable.map((day, dayIdx) => 
                        <tr key={dayIdx} className="text-white text-center border border-white">
                            {day.meals.map((meal, mealIdx) => 
                                <td key={mealIdx} className="border border-white pl-1 pr-1 pt-1 pb-1">
                                    {meal.dishes.map((dish, dishIdx) =>
                                        <div key={dishIdx}>
                                            {dish}
                                        </div>
                                    )}
                                </td>
                            )}
                        </tr>
                    )
                }
            </tbody>
        </table>

     );
}
 
export default OldMenuTable;

