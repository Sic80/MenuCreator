import { RecipeDatabase } from './types/RecipeDataBaseInterface'
import { RecipeDatabaseTypes }  from './types/RecipeDatabaseTypes';
import RecipeDatabaseActionsTypes  from './types/RecipeDatabaseActionsTypes';
import { MenuDatabase } from './types/MenuDatabaseInterface';
import { MenuDatabaseTypes }  from './types/MenuDatabaseTypes';
import MenuDatabaseActionsTypes from './types/MenuDatabaseActionsTypes';
import { SlidingPanelsControl } from './types/SlidingPanelsControlInterface';
import { SlidingPanelsControlTypes } from './types/SlidingPanelsControlTypes';
import SlidingPanelControlsActionsTypes from './types/SlidingPanelsControlActionsTypes';


export const recipeDataBase = (state: RecipeDatabase={recipes: []}, action: RecipeDatabaseTypes): RecipeDatabase => {
    switch(action.type) {        
        case RecipeDatabaseActionsTypes.SAVE_RECIPE: {
            const { recipe } = action.payload;
            let recipesCopy = [...state.recipes, recipe];
            recipesCopy.sort((a, b) => (a.recipeName > b.recipeName) ? 1 : -1);
            return {...state, recipes: recipesCopy}
        }        
        case RecipeDatabaseActionsTypes.DELETE_RECIPE: {
            const { idx } = action.payload;
            let recipesCopy = [...state.recipes.slice(0, idx), ...state.recipes.slice(idx + 1, state.recipes.length)];
            return {...state, recipes: recipesCopy}
        }             
        default: return state;
    }
}

export const menuDatabase = (state: MenuDatabase={oldMenus: [], menuSaved: []}, action: MenuDatabaseTypes): MenuDatabase => {
    switch(action.type){
        case MenuDatabaseActionsTypes.SAVE_MENU: {
            let { date, tableValues } = action.payload;
            const sameDateMenuNr = state.oldMenus.filter(oldMenu => oldMenu.name.split(" ")[0] === date).length;
            if (sameDateMenuNr > 0) {
                date = `${date} - ${sameDateMenuNr + 1}`
            }
            return {...state, oldMenus: [...state.oldMenus, {name: date, table: tableValues}]};
        }
        case MenuDatabaseActionsTypes.DELETE_MENU: {
            const { menuIdx } = action.payload;
            let oldMenusCopy = [...state.oldMenus.slice(0, menuIdx), ...state.oldMenus.slice(menuIdx + 1)];            
            return {...state, oldMenus: [...oldMenusCopy]};
        }        
        case MenuDatabaseActionsTypes.SAVE_MENU_TO_SEND: {
            const { menu } = action.payload;            
            return {...state, menuSaved: menu};
        }
        default: return state;
    }    
}

export const slidingPanelsControl = (state: SlidingPanelsControl={recipesPanels: [false, false], oldMenusPanels: [false, false], editRecipePanel: false, shoppingListPanel: false}, action: SlidingPanelsControlTypes): SlidingPanelsControl => {
    switch(action.type){
        case SlidingPanelControlsActionsTypes.TOGGLE_RECIPES_PANELS: {
            const { idx1, idx2 } = action.payload;
            let recipePanelsCopy = [...state.recipesPanels]
            recipePanelsCopy[idx1] = !recipePanelsCopy[idx1];
            recipePanelsCopy[idx2] = !recipePanelsCopy[idx2];
            return {...state, recipesPanels: recipePanelsCopy}
        }
        case SlidingPanelControlsActionsTypes.TOGGLE_OLD_MENUS_PANELS: {
            const { idx1, idx2 } = action.payload;
            let oldMenusPanelsCopy = [...state.oldMenusPanels]
            oldMenusPanelsCopy[idx1] = !oldMenusPanelsCopy[idx1];
            oldMenusPanelsCopy[idx2] = !oldMenusPanelsCopy[idx2];
            return {...state, oldMenusPanels: oldMenusPanelsCopy}
        }
        case SlidingPanelControlsActionsTypes.TOGGLE_EDIT_RECIPE_PANEL: {
            return {...state, editRecipePanel: !state.editRecipePanel}
        }
        case SlidingPanelControlsActionsTypes.TOGGLE_SHOPPING_LIST_PANEL: {
            return {...state, shoppingListPanel: !state.shoppingListPanel}
        }
        default: return state;
    }
}