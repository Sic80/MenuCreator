import React, { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { useReactToPrint } from 'react-to-print';
import useGetIngredients  from './useGetIngredients';
import { RiDeleteBin7Line } from 'react-icons/ri';
import { FaRegEdit } from 'react-icons/fa';
import ShoppingListNewItemForm from './ShoppingListNewItemForm';
import { ToastContainer, toast, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Ingredient } from './types/IngredientInterface';
import { RootState } from './types/RootState';
import { Recipe } from './types/RecipeInterface';

  


const ShoppingList = () => {    
    
    const _ = require('lodash');
    const componentRef = useRef<HTMLDivElement>(null);
    const handlePrint = useReactToPrint({
        content:() => componentRef.current, 
    });

    const [isIngredientClicked, setisIngredientClicked] = useState(-1);
    const [isIngredientToEdit, setIsIngredientToEdit] = useState(-1);
    const [isNewItem, setIsNewItem] = useState(false);

    const setNewShoppingListItem = (newItem: Ingredient, oldItemIdx: number) => {
        if (isIngredientToEdit >= 0) {
            const shoppingListItemSortedCopy = [...shoppingListItemSorted];
            shoppingListItemSortedCopy[oldItemIdx] = newItem;
            setShoppingListItem(shoppingListItemSortedCopy);
            setIsIngredientToEdit(-1);
        } else {
            setShoppingListItem([...shoppingListItemSorted, newItem]);
            setIsNewItem(false);
        }
    }    
    const recipes = _.cloneDeep(useSelector((state: RootState) => state.recipeDataBase.recipes));
    const menu = useSelector((state: RootState) => state.menuDatabase.menuSaved);
    const menuDishes: string[] = _.cloneDeep(_.flattenDeep(menu.map(day => day.meals.map(meal => meal.dishes))));
    const ingredients = _.cloneDeep(menuDishes.map((dish: string) => recipes.filter((recipe: Recipe) => recipe.recipeName === dish)).flat().map(menuRecipe => menuRecipe.ingredients).flat());
    const [shoppingListItem, setShoppingListItem] = useState(ingredients);   
    const shoppingListItemSorted: Ingredient[] = useGetIngredients(_.cloneDeep(shoppingListItem));

    return ( 
        <div className="text-left  bg-white no-x-overflow">
            <div
                className="text-dark bg-white mt-5 mb-5 ml-3 ml-sm-5" 
                ref={componentRef}
            >
                {shoppingListItemSorted.map((ingredient, ingredientIdx) => 
                <div 
                    key={ingredientIdx}
                    className="row ml-1 mt-1 pointer"
                >
                    {ingredientIdx === isIngredientToEdit ?
                        <ShoppingListNewItemForm 
                            setNewShoppingListItem={setNewShoppingListItem} 
                            oldItemValues={ingredient} 
                            oldItemIdx={ingredientIdx}                        
                        /> 
                    : (
                        <div 
                            className=" w-auto"
                            onClick={() => {
                                if (ingredientIdx === isIngredientClicked) {
                                    setisIngredientClicked(-1)
                                } else {
                                    setisIngredientClicked(ingredientIdx);
                                }
                            }}
                        >
                            {ingredient.name} {ingredient.quantity !== "0" ? ingredient.quantity : ""} {ingredient.unit}
                        </div>
                    )}
                    <div
                        className="col-3"
                        style={{display: isIngredientClicked === ingredientIdx ? "inline" : "none"}}
                    >
                        <FaRegEdit 
                            className="mr-2" 
                            onClick={() => {
                                setIsIngredientToEdit(ingredientIdx);
                                setisIngredientClicked(-1);
                                setIsNewItem(false);
                            }}
                        />
                        <RiDeleteBin7Line 
                            className="" 
                            onClick={() => {
                                setShoppingListItem([...shoppingListItemSorted.slice(0, ingredientIdx), ...shoppingListItemSorted.slice(ingredientIdx + 1)]);
                                setisIngredientClicked(-1);
                            }}
                        />
                    </div>                    
                </div>                    
                )}
                {isNewItem &&
                    <div className="mt-2 row ml-1">
                        <ShoppingListNewItemForm
                            setNewShoppingListItem={setNewShoppingListItem} 
                        />
                    </div>
                }
            </div>
            <div className="ml-5 pb-5">
                <button
                    className="btn bg-black  text-white"
                    onClick={() => {
                        setIsNewItem(!isNewItem);
                        setIsIngredientToEdit(-1);
                    }}
                >
                    {isNewItem ? "Back" : "New item"}
                </button>
                <button 
                    className="btn bg-black  ml-4 text-white"
                    onClick={() =>  {
                        if (!isNewItem && (isIngredientToEdit === -1)) {
                            if(handlePrint) handlePrint();
                        } else {
                            toast.dark("Fill all the fields");
                        }
                        
                    }}
                >
                    Print Shopping List
                </button>                
            </div>
            <ToastContainer 
                position="bottom-center"
                autoClose={4000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                limit={1}
                transition={Slide}            
            />
        </div>
    );
}
 
export default ShoppingList;