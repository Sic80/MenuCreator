
const NotFound = () => {
    return (
        <div className="container-fluid">
            <div className="row d-flex align-content-center">
                <div className="col-2 col-md-3 d-flex justify-content-center">
                    <img className="align-self-center img-fluid" src={require('../image/fork.jpg')} alt="A fork" />
                </div>
                <div className="col-8 col-md-6 bg-black text-white mt-2 text-center text-uppercase font-raleway">
                    <h1 className="big-text-responsive">we can't find</h1>
                    <h1 className="big-text-responsive">the page you are looking for.</h1>
                    <h1 className="big-text-responsive">sorry for the inconvenient.</h1>
                </div>
                <div className="col-2 col-md-3 d-flex justify-content-center">
                    <img className="align-self-center img-fluid" src={require('../image/knife.jpg')} alt="A knife" />
                </div>
            </div>
        </div>
    )
}

export default NotFound