export default function useGetRangeOfDays (s: Date , e: Date) {
    let a = [];
    for(let d = new Date(s); d <= e; d.setDate(d.getDate() + 1)){ 
        a.push(new Date(d).toLocaleDateString('en-US', { day: 'numeric', weekday: 'long' }));
    }
    return a;
};
