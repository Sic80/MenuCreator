import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SlidingPanel from 'react-sliding-side-panel';
import { saveMenuToSend ,toggleOldMenusPanels } from './actions';
import OldMenuTable from './OldMenuTable';
import { ImArrowRight } from 'react-icons/im'
import { RootState } from './types/RootState';
import { OldMenu } from './types/OldMenuInterface';



const OldMenusDatabase = () => {

    const dispatch = useDispatch();
    const menuDatabase = useSelector((state: RootState) => state.menuDatabase.oldMenus);
    const oldMenusPanels = useSelector((state: RootState) => state.slidingPanelsControl.oldMenusPanels);
    const [oldMenu, setOldMenu] = useState<OldMenu>({name: "", table: []});


    return (    
        <div className="row mt-5 d-flex justify-content-center ">
            <div className="col-10 text-center">                
                <div className=" bg-black text-white border border-white overflow-auto vh-80 sliding-database pt-2">
                    {menuDatabase.length === 0 ? 
                        <div className="mt-4 h5">No menu has been saved</div>
                    :                    
                        (menuDatabase.map((menu, menuIdx) =>
                            <div 
                                key={menuIdx}
                                className="font-montserrat mt-2 mb-2"
                                >
                            <span
                                    className="w-auto pointer"
                                    onClick={() => {
                                        dispatch(toggleOldMenusPanels(1));
                                        setOldMenu(menu);  
                                    }}
                            >
                                {menu.name}
                            </span>  
                            </div>
                        ))
                    }
                </div>
            </div>
            <SlidingPanel 
                    type={"right"}
                    isOpen={oldMenusPanels[1]}
                    noBackdrop={true}
                    size={25}
                    panelContainerClassName=" bg-black col-12 col-sm-8 col-md-6 col-lg-6"

                >
                    <div className="bg-black h-100 overflow-hidden">
                        <div 
                            className="col text-white mt-5 mb-3 h5 pointer text-center border-bottom border-white pb-2"
                            onClick={() => dispatch(toggleOldMenusPanels(1))}
                        > 
                             <ImArrowRight size={25}/>
                        </div>
                        <div className="text-center mb-5 text-white font-montserrat">
                             {oldMenu.name || "No menu has been selected"} 
                        </div>
                        <div className="row d-flex justify-content-center">
                            <div className="col-8"></div>                            
                                {oldMenu.name &&
                                    <OldMenuTable 
                                        oldMenuTable={oldMenu.table}
                                        tableWidth={"col-10"}
                                    />
                                }
                        </div>
                        <div className="row mt-2 d-flex justify-content-center ">
                            {oldMenu.name &&
                                <button 
                                className="btn bg-black border-white text-white mt-3 font-roboto"
                                onClick={() => {
                                    dispatch(saveMenuToSend(oldMenu.table));
                                    dispatch(toggleOldMenusPanels(0,1));                                 
                                }}
                                >
                                    Use this menu
                                </button>
                            }
                        </div>                             
                    </div>
                </SlidingPanel>
        </div>
      );
}
 
export default OldMenusDatabase;

