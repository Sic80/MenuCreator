import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SlidingPanel from 'react-sliding-side-panel';
import { toggleRecipesPanels } from './actions';
import { ImArrowLeft } from 'react-icons/im'
import { RootState } from './types/RootState';
import { Recipe } from './types/RecipeInterface';



const RecipesDatabase = () => {

    const dispatch = useDispatch();
    const recipes = useSelector((state: RootState) => state.recipeDataBase.recipes);
    const [recipe, setRecipe] = useState<Recipe>();
    const recipesPanels = useSelector((state: RootState) => state.slidingPanelsControl.recipesPanels);

    


    return (    
        <div className="row mt-5 d-flex justify-content-center ">
            <div className="col-10 text-center">
                <div className="border bg-black text-white overflow-auto pt-2 sliding-database">
                    {recipes.length === 0 ?
                        <div>No recipe has been saved</div>
                     :                    
                        (recipes.map((recipe, recipeIdx) => 
                            <div 
                                key={recipeIdx} 
                                className="dropdown mt-2 mb-2 d-flex justify-content-center"                            
                            >
                                <div 
                                    className="pointer w-auto font-montserrat pl-1 pr-1" 
                                    onClick={() => {
                                        dispatch(toggleRecipesPanels(1));
                                        setRecipe(recipe);
                                    }}
                                >
                                    {recipe.recipeName}                                  
                                </div>
                            </div>
                        ))
                    }
                </div>
                <SlidingPanel 
                    type={"left"}
                    size={0}
                    isOpen={recipesPanels[1]}
                    noBackdrop={true}
                    panelContainerClassName=" bg-black col-12 col-sm-8 col-md-6 col-lg-4"

                >
                    <div
                        className="bg-black h-100 text-white overflow-y-auto"
                    >
                        <div 
                            className="col text-white mt-5 mb-3 h5 pointer text-center w-auto border-bottom border-white pb-2"
                            onClick={() => dispatch(toggleRecipesPanels(1))}
                        > 
                            <ImArrowLeft size={25}/> 
                        </div>
                            <div className="mt-2 mb-5 h6 font-montserrat">
                                {recipe ? recipe.recipeName : "No recipe has been selected"}
                                
                            </div>
                        <div>                                                                
                            {recipe && recipe.ingredients.map((ingredient, ingredientIdx) =>
                                <div key={ingredientIdx} className="font-roboto">
                                    {ingredient.name} {ingredient.quantity !== "0" ? ingredient.quantity : ""} {ingredient.unit}
                                </div>
                            )}
                        </div>
                    </div>
                </SlidingPanel>
            </div>
        </div>
    );
}
 
export default RecipesDatabase;

