import { useState, useRef, useEffect } from 'react';
import { Ingredient } from './types/IngredientInterface';

interface ShoppingListNewItemFormProps {
    setNewShoppingListItem: Function,
    oldItemValues?: Ingredient,
    oldItemIdx?: number
}

const ShoppingListNewItemForm = ({ setNewShoppingListItem, oldItemValues, oldItemIdx }: ShoppingListNewItemFormProps) => {
    
    const selectOptions: string[] = ["Kg", "gr", "L", "mL", "tbsp", "tsp", "pcs", "TT"];
    const [newItemInputsValues, setNewItemInputsValues]= useState<Ingredient>({name: "", quantity: "", unit: ""});
    
    useEffect(() => {
        const _ = require('lodash');
        if (oldItemValues) {
            setNewItemInputsValues(_.cloneDeep(oldItemValues));
        }        
    }, [oldItemValues])

    const setInput = (value: string, input: string) => {
        const newItemInputsValuesCopy = {...newItemInputsValues, [input]: value}
        setNewItemInputsValues(newItemInputsValuesCopy);
    };

    const ref = useRef<HTMLInputElement>(null);
    useEffect(() =>{ 
        if(ref.current) {ref.current.focus()};
    }, []);

    const areInputsEmpty = Object.values(newItemInputsValues).includes("");

    return (
            <div className="form-group row-cols-3 row-cols-sm-4 d-inline mt-1">
                    <input
                        ref={ref}
                        className="col-4 col-sm-6 col-md-6 col-lg-3"
                        autoComplete="off"
                        spellCheck="false"
                        name="name" 
                        placeholder="name" 
                        type="text" 
                        value={newItemInputsValues.name.toLowerCase()}
                        onChange={(e) => setInput(e.target.value, "name")}
                    />
                    <input
                        className="col-2 col-sm-2 col-md-2 col-lg-2" 
                        autoComplete="off"
                        spellCheck="false"
                        name="quantity" 
                        placeholder="qty" 
                        type="text" 
                        value={newItemInputsValues.quantity.replace(",", ".")}
                        onChange={(e) => setInput(e.target.value, "quantity")}
                    />
                    <select  
                        className="col-3 col-sm-2 col-md-2 col-lg-2"
                        name="unit" 
                        value={newItemInputsValues.unit}
                        onChange={event => setInput(event.target.value, "unit")}
                    >
                        <option value="">unit</option>
                        {selectOptions.map((option, optIdx) =>
                            <option key={optIdx} value={option}>{option}</option> 
                        )}
                    </select>
                <button
                    className="col-2 btn bg-black text-white button-sel"
                    onClick={() => setNewShoppingListItem(newItemInputsValues, oldItemIdx)}
                    disabled={areInputsEmpty}
                >
                    <p className="mt-n1">{oldItemValues ? "Save" : "Add"}</p>
                </button>
            </div>
     );
}
 
export default ShoppingListNewItemForm;