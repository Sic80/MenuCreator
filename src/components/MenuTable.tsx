import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { saveMenu, saveMenuToSend, toggleShoppingListPanel } from './actions';
import DropDown from './DropDown';
import { BsPlusCircle } from 'react-icons/bs'
import { FiMinusCircle } from 'react-icons/fi'
import '../css/Navbar.css'
import { useReactToPrint } from 'react-to-print';
import { AiOutlineDelete } from 'react-icons/ai';
import Fade from 'react-reveal/Fade';
import { ToastContainer, toast, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RootState } from './types/RootState';
import { Table } from './types/TableInterface';

interface MenuTableProps {
    rangeOfDays: string[] 
}

const MenuTable = ({ rangeOfDays }: MenuTableProps) => {

    const _ = require('lodash');
    const componentRef = useRef<HTMLDivElement>(null);
    const handlePrint = useReactToPrint({
        content: () => componentRef.current, 
    });
    const dispatch = useDispatch();
    const recipes = useSelector((state: RootState) => state.recipeDataBase.recipes);
    const menuFromOldTable = useSelector((state: RootState) => state.menuDatabase.menuSaved);
    const [inputOnFocus, setInputOnFocus] = useState<string>("");        
    const [tableValues, setTableValues] = useState<Table[]>([]);
    const isSomeInputWrong: boolean = _.flattenDeep(tableValues.map((day: Table) => day.meals.map(meal => meal.errors.map(error => error ===true)))).some((error: boolean) => error === true); 
    const isSomeInputEmpty: boolean = _.flattenDeep(tableValues.map((day: Table) => day.meals.map(meal => meal.dishes.map(dish => dish ==="")))).some((empty: boolean) => empty === true); 
    const isNoCalendarSelection: boolean = rangeOfDays.length === 0;

    useEffect(() => {  
        if (menuFromOldTable) {
            setTableValues(menuFromOldTable);
        }        
    }, [menuFromOldTable]);

    useEffect(() => {
        const createInitialState = (numberOfDays: number) => {
            const initialState = [];
            for (let i = 0; i < numberOfDays; i++) {
                initialState.push({meals: [{name: "lunch", dishes: [""], errors: [false]},{name:"dinner", dishes: [""], errors: [false]}]});
            };
            return initialState;
        }    
        setTableValues(createInitialState(rangeOfDays.length));
    }, [rangeOfDays]);


    const setValue = (day: number, meal: number, inputIdx: number, recipe: string) => {
        let tableValuesCopy =  _.cloneDeep(tableValues);
        tableValuesCopy[day].meals[meal].dishes[inputIdx] = recipe;
        setTableValues(tableValuesCopy);
    }

    const setError = (day: number, meal: number, inputIdx: number, isError: boolean) => {
        let tableValuesCopy = _.cloneDeep(tableValues);
        tableValuesCopy[day].meals[meal].errors[inputIdx] = isError;
        setTableValues(tableValuesCopy);
    }
    
    const accordMessageToError = (isSuccess?: boolean) => {
        let errorMessage = "";
        if (isNoCalendarSelection) {
            errorMessage = "Please, select a range of days for your menu";
        } else if (isSomeInputWrong || isSomeInputEmpty) {
            errorMessage = "Please, fill all the inputs with a recipe present in your database"
        } else if (isSuccess) {
            errorMessage = "Saved";
        }
        return errorMessage;
    }
    return ( 
        <div>
            <div ref={componentRef}>
                <Fade bottom>
                    <table className="table bg-white w-100" >
                        <tbody>
                            <tr>
                                {["Day", "Lunch", "Dinner"].map((name, idx) => <th key={idx} className="menu_table-th border-responsive">{name}</th>)}
                            </tr>                
                            {tableValues.map((day, dayIdx) =>
                                <tr key={dayIdx}>
                                    <td className="border-responsive text-center">
                                        <h6 className="mt-2 ml-n2 ml-sm-0 mr-n2 mr-sm-0">
                                            {
                                                <div>
                                                    <div>{rangeOfDays[dayIdx] && rangeOfDays[dayIdx].split(" ")[0]} </div>
                                                    <div>{rangeOfDays[dayIdx] && rangeOfDays[dayIdx].split(" ")[1].substring(0,3)}</div>
                                                </div> || 
                                                <AiOutlineDelete 
                                                    size={30} 
                                                    className="pointer"
                                                    onClick={() => {
                                                        let tableValuesCopy = _.cloneDeep(tableValues);
                                                        tableValuesCopy = [...tableValuesCopy.slice(0, dayIdx), ...tableValuesCopy.slice(dayIdx + 1)];
                                                        setTableValues(tableValuesCopy);
                                                    }}/> 
                                            }
                                        </h6>
                                    </td>
                                    {day.meals.map((meal, mealIdx) =>
                                        <td key={mealIdx} className="border-responsive text-center">
                                            {meal.dishes.map((dish, dishIdx) =>
                                                <div key={dishIdx} className="w-auto ml-n2 ml-sm-4 mr-n2 mr-sm-4">
                                                    <input 
                                                        className= {meal.errors[dishIdx] ? "no-padding w-100 col-12 border-danger text-black form-control mb-1 text-center" : " no-padding w-100 border text-black form-control mb-1 text-center"}
                                                        autoComplete="off"
                                                        spellCheck="false"
                                                        placeholder="Enter a recipe" 
                                                        name="dish"
                                                        type="text"
                                                        onFocus={() => setInputOnFocus(`${dayIdx}-${mealIdx}-${dishIdx}`)} 
                                                        onBlur={() => {
                                                            if (!recipes.some(recipe => recipe.recipeName === dish.trim())){
                                                                setError(dayIdx, mealIdx, dishIdx, true)
                                                            } else {
                                                                setError(dayIdx, mealIdx, dishIdx, false)
                                                            }
                                                            setInputOnFocus("");
                                                        }}
                                                        onChange={(e) => setValue(dayIdx, mealIdx, dishIdx, e.target.value)}
                                                        value={_.capitalize(dish).replace(/^\s/, "").replace(/ {1,}/g, " ")}
                                                    />
                                                    <div className="w-100 d-flex justify-content-center no-padding">
                                                        <DropDown 
                                                            inputOnFocus={inputOnFocus}
                                                            database={recipes}
                                                            inputValue={dish}
                                                            dayIdx={dayIdx}
                                                            mealIdx={mealIdx}
                                                            dishIdx={dishIdx}
                                                            setValue={setValue}
                                                        />
                                                    </div>
                                                </div>
                                            )}      
                                            <div>
                                                <span
                                                    className="pointer"
                                                    onClick={() => {
                                                        let tableValuesCopy = _.cloneDeep(tableValues);
                                                        tableValuesCopy[dayIdx].meals[mealIdx].dishes.push("");
                                                        setTableValues(tableValuesCopy);
                                                    }} 
                                                >
                                                    <BsPlusCircle className="mr-2" />
                                                </span> 
                                                <span className="text-black">dish</span>
                                                <span
                                                    className="pointer"
                                                    onClick={() => {
                                                        if (meal.dishes.length >= 2) {
                                                            let tableValuesCopy = _.cloneDeep(tableValues);
                                                            tableValuesCopy[dayIdx].meals[mealIdx].dishes.pop();
                                                            setTableValues(tableValuesCopy);
                                                        }
                                                    }}
                                                >
                                                    <FiMinusCircle className="ml-2"/>
                                                </span>                                    
                                            </div>                              
                                        </td>                        
                                    )}
                                </tr>
                            )}                   
                        </tbody> 
                    </table> 
                </Fade>
            </div>
            <div className="mt-5 pt-5">
                <button 
                    onClick={() => {
                        if (!(isSomeInputWrong || isSomeInputEmpty || isNoCalendarSelection)) {
                            if(handlePrint) handlePrint();
                        } else {
                            toast.dark(accordMessageToError());
                        }
                    }}
                    className="btn bg-black border-white text-white font-roboto"
                >
                    Print menu
                </button>
                <button
                    className="btn bg-black border-white text-white ml-1 font-roboto"
                    onClick={() => {
                        if (!(isSomeInputWrong || isSomeInputEmpty || isNoCalendarSelection)) {
                            dispatch(saveMenu(new Date().toLocaleDateString(), tableValues))
                            toast.dark(accordMessageToError(true))
                        } else {
                            toast.dark(accordMessageToError())
                        }
                    }}
                >
                    Save menu
                </button>
            </div>
            <div className="row d-flex no-gutters w-100 justify-content-center">
                <button 
                    className="btn col-10 col-md-6 col-lg-4 bg-black text-white border-white mt-5 mb-3 pt-3 pb-3 font-roboto"
                    onClick={() => {
                        if (!(isSomeInputWrong || isSomeInputEmpty || isNoCalendarSelection)) {
                            dispatch(toggleShoppingListPanel());
                            dispatch(saveMenuToSend(tableValues));
                        } else {
                            toast.dark(accordMessageToError())
                        }
                    }}
                >
                    <div className="h5 pt-2">
                        Get Shopping List       

                    </div>
                </button>
            </div>
            <ToastContainer 
                    position="bottom-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    limit={1}
                    transition={Slide}            
                />
        </div>
     );
}
 
export default MenuTable;