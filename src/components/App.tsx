import { Route, Switch } from 'react-router-dom';
import HomePage from './HomePage';
import AddNewRecipe from './AddNewRecipe';
import RecipesAndMenusDatabase from './RecipesAndMenusDatabase';
import NotFound from './NotFound';


const App = () => {
    return(
        <div>
            <Switch>                
                <Route exact path='/' component={HomePage}></Route>
                <Route exact path='/add-new-recipe' component={AddNewRecipe}></Route>
                <Route exact path='/database' component={RecipesAndMenusDatabase}></Route>
                <Route component={NotFound} />

            </Switch>
        </div>
    );
}

                                        
export default App;
