import React, { useState } from 'react';
import Navbar from './Navbar';
import { useSelector, useDispatch } from 'react-redux';
import DatabaseRecipeContent from './DatabaseRecipeContent';
import DatabaseOldMenusContent from './DatabaseOldMenusContent';
import SlidingPanel from 'react-sliding-side-panel';
import { AiOutlineCloseCircle } from 'react-icons/ai'
import RecipeForm from './RecipeForm';
import { toggleEditRecipePanel } from './actions';
import { ToastContainer, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RootState } from './types/RootState';


const RecipesAndMenusDatabase = () => {

    const dispatch = useDispatch();
    const menuDatabase = useSelector((state: RootState) => state.menuDatabase);
    const recipes = useSelector((state: RootState) => state.recipeDataBase.recipes);
    const [recipeToEditIdx, setRecipeToEditIdx] = useState<number>(0);
    const editRecipePanel = useSelector((state: RootState) => state.slidingPanelsControl.editRecipePanel);
    const setRecipeToEditIdxFromChild = (idx: number) => {
        setRecipeToEditIdx(idx);
        console.log(idx)
    }
    

 
    return (    
        <div className="bg-black text-white no-x-overflow">            
            <div className="fixed-top mb-5">
                <Navbar/>
            </div>
            <div className="row row-cols-1 row-cols-lg2 mt-5 mb-5 justify-content-center pt-5">
                <div className="col-10 col-lg-5 text-center mb-5">
                    <div className="big-text-responsive font-raleway">
                        Recipes
                    </div>
                    <div className="border border-white mt-2 pt-4 pb-4 database overflow-auto">
                        {recipes.length === 0 ?
                            <div>
                                <div className="font-roboto h5">No recipe has been saved</div>
                                <img className="align-self-center img-fluid mt-2" src={require('../image/fork.jpg')} alt="A fork" />
                            </div>
                        :
                            recipes.map((recipe, recipeIdx) => 
                                <DatabaseRecipeContent
                                    recipe={recipe}
                                    recipeIdx={recipeIdx}
                                    key={recipeIdx}
                                    setRecipeToEditIdxFromChild={setRecipeToEditIdxFromChild}
                                />
                            )
                        }
                        <SlidingPanel 
                            type={"bottom"}
                            isOpen={editRecipePanel}
                            size={90}
                        >
                            <div
                                className="bg-black h-100 no-x-overflow"
                            >
                                <div 
                                    className="col-1 text-white ml-3 mt-5 pt-4 h3 pointer bg-black"
                                    onClick={() => {
                                        dispatch(toggleEditRecipePanel());                                            
                                    }}
                                > 
                                    <AiOutlineCloseCircle size={40} />
                                </div>
                                <div className="col-12 d-flex justify-content-center mt-sm-5">
                                    <RecipeForm 
                                        initialState={recipes[recipeToEditIdx]} 
                                        isToEdit={[true, recipeToEditIdx]} 
                                    />
                                </div>
                            </div>
                        </SlidingPanel>
                    </div>
                </div>
                <div className="col-10 col-lg-5 text-center">
                    <div className="big-text-responsive font-raleway">
                        Old menus
                    </div>
                    <div className="border border-white mt-2 pb-4 pt-4 database overflow-auto">
                        {menuDatabase.oldMenus.length === 0 ?
                            <div>
                                <div className="font-roboto h5">No menu has been saved</div>
                                <img className="align-self-center img-fluid mt-2" src={require('../image/knife.jpg')} alt="A knife" />
                            </div>
                        :
                            menuDatabase.oldMenus.map((menu, menuIdx) =>
                                <DatabaseOldMenusContent
                                    menu={menu}
                                    menuIdx={menuIdx}
                                    key={menuIdx}
                                />
                            )
                        }
                    </div>
                </div>
                <ToastContainer 
                    position="bottom-center"
                    autoClose={2500}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    limit={1}
                    transition={Slide} 
                    bodyClassName={"text-center w-100"}                          
                />
            </div>
        </div>
      );
}
 
export default RecipesAndMenusDatabase;

