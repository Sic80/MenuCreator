import React, { useState } from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import SlidingPanel from 'react-sliding-side-panel';
import NavbarLinks from './NavbarLinks';



const Navbar = () => {

    const [isHamburgerOpen, setIsHamburgerOpen] = useState<boolean>(false);
    
    return (
         
        <nav className="navbar navbar-expand-sm bg-black pt-sm-4 pb-sm-4 border-bottom border-white">
            <div className="row row-cols-3 d-sm-none w-100 no-gutters">
                <div className="navbar-toggler col-2 d-flex align-items-center justify-content-center button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <GiHamburgerMenu 
                        color={"white"}
                        onClick={() => setIsHamburgerOpen(!isHamburgerOpen)}
                    />
                </div>
                <div className="navbar-brand d-xs-block d-sm-none text-white col-8 font-montserrat text-center">
                    Menu Creator
                </div>
                <div className="col-1 d-xs-block d-sm-none"></div>

            </div>
            <NavbarLinks cls={"navbar-nav row-cols-3 d-none d-sm-flex w-100 mt-sm-n2 mb-sm-n3 mt-md-0 mb-md-0"}/>
            <SlidingPanel 
                type={"left"}
                isOpen={isHamburgerOpen}                    
                panelContainerClassName=" bg-black col-8 text-white overflow-hidden"
                backdropClicked={() => setIsHamburgerOpen(!isHamburgerOpen)}
                size={0}
            >
                
                <NavbarLinks cls={"navbar-nav row-cols-3 d-flex d-sm-none w-100 mt-5"} />
            </SlidingPanel>
        </nav>
     );
}
 
export default Navbar;