import { useHistory, useLocation } from 'react-router-dom';

interface NavbarLinksProps {
    cls: string
}

const NavbarLinks = ({ cls }: NavbarLinksProps) => {

    const history = useHistory();
    const location = useLocation();

    return ( 
        <div className={cls}>                
            <div className="nav-item active col-10 col-sm-4 order-3 order-sm-1 mt-2 mt-sm-0">
                <div
                    className={location.pathname === "/database" ? "text-white text-left text-sm-center pointer h4 position-sm-fixed font-montserrat " : "text-white text-left position-sm-fixed text-sm-center pointer h5 font-montserrat"}
                    onClick={() => history.push('/database')}
                >
                    Database       
                </div>
            </div>
            <div className="nav-item active col-10  col-sm-4 order-1 order-sm-2 mt-3 mt-sm-0">
                <div
                    className={location.pathname === "/" ? "text-white text-left text-sm-center pointer h4 position-sm-fixed font-montserrat " : "text-white text-left position-sm-fixed text-sm-center pointer h5 font-montserrat"}
                    onClick={() => history.push('/')}
                >
                    Home        
                </div>
            </div>
            <div className="nav-item active col-10 col-sm-4 order-2 order-sm-3 mt-2 mt-sm-0 ">
                <div
                    className={location.pathname === "/add-new-recipe" ? "text-white text-left text-sm-center pointer h4 position-sm-fixed font-montserrat " : "text-white text-left position-sm-fixed text-sm-center pointer h5 font-montserrat"}
                    onClick={() => {
                        history.push('/add-new-recipe');
                    }}
                >
                    New recipe
                </div>
            </div>
        </div>
     );
}
 
export default NavbarLinks;