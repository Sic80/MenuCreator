
import { OldMenu } from './OldMenuInterface';
import { Table } from './TableInterface';

export interface MenuDatabase {
    oldMenus: OldMenu[],
    menuSaved: Table[]
}