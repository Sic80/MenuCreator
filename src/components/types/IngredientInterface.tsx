export interface Ingredient {
    [key: string]: string,
    quantity: string,
    unit: string
}