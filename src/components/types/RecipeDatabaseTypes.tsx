import RecipeDatabaseActionsTypes from './RecipeDatabaseActionsTypes';
import { Recipe } from './RecipeInterface';

interface saveRecipeAction {
    type: typeof RecipeDatabaseActionsTypes.SAVE_RECIPE,
    payload: {recipe: Recipe}
}

interface deleteRecipeAction {
    type: typeof RecipeDatabaseActionsTypes.DELETE_RECIPE,
    payload: {idx: number}
}

export type RecipeDatabaseTypes = saveRecipeAction | deleteRecipeAction;