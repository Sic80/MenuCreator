import { Meal } from './MealInterface';

export interface Table {
    meals: Meal[]
}