export interface SlidingPanelsControl {
    recipesPanels: boolean[],
    oldMenusPanels: boolean[],
    editRecipePanel: boolean,
    shoppingListPanel: boolean
}