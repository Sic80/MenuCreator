import SlidingPanelControlsActionsTypes from "./SlidingPanelsControlActionsTypes";


interface toggleRecipesPanelsAction {
    type: typeof SlidingPanelControlsActionsTypes.TOGGLE_RECIPES_PANELS,
    payload: {idx1: number, idx2: number}
}

interface toggleOldMenusPanelsAction {
    type: typeof SlidingPanelControlsActionsTypes.TOGGLE_OLD_MENUS_PANELS,
    payload: {idx1: number, idx2: number}
}

interface toggleShoppingListPanelAction {
    type: typeof SlidingPanelControlsActionsTypes.TOGGLE_SHOPPING_LIST_PANEL,
}

interface toggleEditRecipePanelAction {
    type: typeof SlidingPanelControlsActionsTypes.TOGGLE_EDIT_RECIPE_PANEL,
}

export type SlidingPanelsControlTypes = toggleRecipesPanelsAction | toggleOldMenusPanelsAction | toggleShoppingListPanelAction | toggleEditRecipePanelAction