export interface Meal {
    name: string,
    dishes: string[],
    errors: boolean[]
}