import { Recipe } from './RecipeInterface';

export interface RecipeDatabase {
    recipes: Recipe[]
}