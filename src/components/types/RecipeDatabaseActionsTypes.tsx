enum RecipeDatabaseActionsTypes {
    SAVE_RECIPE = "SAVE_RECIPE",
    DELETE_RECIPE = "DELETE_RECIPE"
}

export default RecipeDatabaseActionsTypes;