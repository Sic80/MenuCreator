import { RecipeDatabase } from './RecipeDataBaseInterface';
import { MenuDatabase } from './MenuDatabaseInterface';
import { SlidingPanelsControl } from './SlidingPanelsControlInterface';


export interface RootState {
    recipeDataBase: RecipeDatabase,
    menuDatabase: MenuDatabase,
    slidingPanelsControl: SlidingPanelsControl
}