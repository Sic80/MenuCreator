import MenuDatabaseActionsTypes from "./MenuDatabaseActionsTypes";
import { Table } from './TableInterface';


interface saveMenuAction {
    type: typeof MenuDatabaseActionsTypes.SAVE_MENU,
    payload: {date: string, tableValues: Table[]}
}

interface deleteMenuAction {
    type: typeof MenuDatabaseActionsTypes.DELETE_MENU,
    payload: {menuIdx: number} 
}

interface saveMenuToSendAction {
    type: typeof MenuDatabaseActionsTypes.SAVE_MENU_TO_SEND,
    payload: {menu: Table[]}
}

export type MenuDatabaseTypes = saveMenuAction | deleteMenuAction | saveMenuToSendAction