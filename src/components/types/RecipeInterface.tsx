import { Ingredient } from './IngredientInterface';

export interface Recipe {
    recipeName: string,
    ingredients: Ingredient[]
}

