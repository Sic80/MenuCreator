enum MenuDatabaseActionsTypes {
    SAVE_MENU = "SAVE_MENU",
    DELETE_MENU = "DELETE_MENU",
    SAVE_MENU_TO_SEND = "SAVE_MENU_TO_SEND"
}

export default MenuDatabaseActionsTypes;