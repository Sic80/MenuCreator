import { Recipe } from './types/RecipeInterface';

interface DropDownProps {
    inputOnFocus: string,
    database: Recipe[],
    inputValue: string,
    dayIdx: number,
    mealIdx: number, 
    dishIdx: number, 
    setValue: Function 
}

const DropDown = ({ inputOnFocus, database, inputValue, dayIdx, mealIdx, dishIdx, setValue }: DropDownProps) => {

    let input = inputValue
    let dropDownContent: string[] = database.map((recipe: Recipe) => recipe.recipeName).filter((recipeName: string) => recipeName.slice(0, input.length).toLowerCase() === input.toLowerCase() && !(recipeName === input));

    const areStillDifferentOptions: boolean = !(dropDownContent.length === 1 && dropDownContent[0] === inputValue);
    const isCurrentInputOnFocus: boolean = inputOnFocus === `${dayIdx}-${mealIdx}-${dishIdx}`;
    const isCurrentInputEmpty: boolean = input === "";
    const isDropDownNotEmpy: boolean = dropDownContent.length > 0;
    const hasToBeDisplayed: boolean = (isCurrentInputOnFocus && !isCurrentInputEmpty) && isDropDownNotEmpy && areStillDifferentOptions;

    return ( 
        <div  
            style={{display: hasToBeDisplayed ? "block" : "none"}}
            className=" position-absolute pb-3 border border-dark bg-black text-white pt-3 col-4 w-100  no-padding dropdown-height overflow-auto"
        >
            {dropDownContent.map((recipe, idx) => 
                <div 
                    key={idx}
                    className="pointer  pb-1 pt-1"
                    onMouseDown={() => setValue(dayIdx, mealIdx, dishIdx, recipe)}
                >
                    {recipe}
                </div>
            )}
        </div>
     );
}

export default DropDown;
 
