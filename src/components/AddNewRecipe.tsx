import React from 'react';
import Navbar from './Navbar';
import RecipeForm from './RecipeForm';
import { Recipe } from './types/RecipeInterface';
import { Ingredient } from './types/IngredientInterface';


const AddNewRecipe = () => {

    const initialIngredientState: Ingredient =  {name: "", quantity: "", unit: ""};
    const initialState: Recipe = {recipeName: "", ingredients: [{...initialIngredientState}]}

    return(
        <div  className="bg-black vh-100 no-x-overflow">
            <div className="fixed-top mb-5">
                <Navbar/>
            </div>
            <div className="row row-cols-1 row-cols-md-2 pt-5 mt-5">
                <div className="col-12 col-md-6 font-raleway text-white d-flex justify-content-center mt-sm-5">
                    <div className="text-left big-text-responsive ml-sm-5">
                        <div className="">ENTER RECIPE</div>
                        <div className=""> NAME AND</div>
                        <div className="">INGREDIENTS</div>
                    </div>
                </div>
                <div className="col-12 col-md-6 d-flex justify-content-center mt-sm-5">
                    <div>
                        <RecipeForm initialState={initialState} isToEdit={[false, -1]} />
                    </div>
                </div>
            </div>            
        </div>
    );                
}

export default AddNewRecipe;