import { Ingredient } from './types/IngredientInterface';

const useGetingredients = (ingredients: Ingredient[]) => {
    const _ = require('lodash');
    let ingredientsCopy = _.cloneDeep(ingredients);
    ingredientsCopy.sort((a: Ingredient, b: Ingredient) => (a.name > b.name) ? 1 : -1)
    for (let i = 1; i < ingredientsCopy.length; i++) {
        if (ingredientsCopy[i-1].name.replaceAll(" ", "") === ingredientsCopy[i].name.replaceAll(" ", "")) {
            if (ingredientsCopy[i].unit === ingredientsCopy[i-1].unit) {
                const quantityCopy = (parseFloat(ingredientsCopy[i].quantity) + parseFloat(ingredientsCopy[i - 1].quantity)).toString();
                ingredientsCopy[i] = {...ingredientsCopy[i], quantity: quantityCopy};
                ingredientsCopy = [...ingredientsCopy.slice(0, i - 1), ...ingredientsCopy.slice(i)]
                i -= 1;           
            }      
            else if ((ingredientsCopy[i].unit === "Kg" && ingredientsCopy[i-1].unit === "gr") || (ingredientsCopy[i].unit === "L" && ingredientsCopy[i-1].unit === "mL")) {
                const quantityCopy = (parseFloat(ingredientsCopy[i].quantity) + parseFloat(ingredientsCopy[i - 1].quantity) / 1000).toString();
                ingredientsCopy[i] = {...ingredientsCopy[i], quantity: quantityCopy};
                ingredientsCopy = [...ingredientsCopy.slice(0, i - 1), ...ingredientsCopy.slice(i)]
                i -= 1;
            } else if ((ingredientsCopy[i].unit === "gr" && ingredientsCopy[i-1].unit === "Kg") || (ingredientsCopy[i].unit === "mL" && ingredientsCopy[i-1].unit === "L")) {
                const quantityCopy = (parseFloat(ingredientsCopy[i].quantity) / 1000 + parseFloat(ingredientsCopy[i - 1].quantity)).toString();
                ingredientsCopy[i] = {...ingredientsCopy[i], quantity: quantityCopy, unit: ingredientsCopy[i-1].unit};
                ingredientsCopy = [...ingredientsCopy.slice(0, i - 1), ...ingredientsCopy.slice(i)]
                i -= 1;
            }
        }
        if (ingredientsCopy[i].unit === "gr" && ingredientsCopy[i].quantity >= 1000) {
            const quantityCopy = (ingredientsCopy[i].quantity / 1000).toString();
            ingredientsCopy[i] = {...ingredientsCopy[i], quantity: quantityCopy, unit: "Kg"};
        } else if (ingredientsCopy[i].unit === "ml" && ingredientsCopy[i].quantity >= 1000) {
            const quantityCopy = (ingredientsCopy[i].quantity / 1000).toString();
            ingredientsCopy[i] = {...ingredientsCopy[i], quantity: quantityCopy, unit: "L"};
        }
    }
    return _.cloneDeep(ingredientsCopy);
}
 
export default useGetingredients;