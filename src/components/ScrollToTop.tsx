import { useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";

export default function ScrollToTop() {
  const { pathname } = useLocation();
  const { action } = useHistory();
  
  useEffect(() => {
    if (action !== "POP") {      
      window.scrollTo(0, 0);
    }
  }, [pathname, action]);

  return null;
}