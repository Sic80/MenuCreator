import React, { useState, useRef, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { deleteMenu, saveMenu } from './actions';
import OldMenuTable from './OldMenuTable';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin7Line } from 'react-icons/ri';
import { OldMenu } from './types/OldMenuInterface';

interface DomcProps {
    menu: OldMenu,
    menuIdx: number
}

const DatabaseOldMenusContent = ({ menu, menuIdx }: DomcProps) => {

    const dispatch = useDispatch();
    const [menuNameToEdit, setMenuNameToEdit] = useState<number>(-1);
    const inputRef = useRef<HTMLInputElement>(null);
    useEffect(() => {
        if (menuNameToEdit !== -1) {
            if (inputRef.current) {inputRef.current.focus()};
        }
    }, [menuNameToEdit])
    const [newMenuName, setNewMenuName] = useState<string>("");

    return ( 
        <div className="container-fluid">
            {menuNameToEdit === menuIdx ?
                (
                    <div className="d-flex justify-content-center h5">
                        <input 
                            ref={inputRef}
                            className="text-center input input-h "
                            type="text"
                            value={newMenuName}
                            onChange={(e) => setNewMenuName(e.target.value)}
                        />
                        <button
                            className="button bg-black border-white text-white btn input-h ml-2"
                            onClick={() => {
                                dispatch(saveMenu(newMenuName, menu.table));
                                dispatch(deleteMenu(menuIdx))
                                setMenuNameToEdit(-1)
                            }}
                        >
                            Save
                        </button>                                         
                    </div>
                ) 
                : 
                ( 
                    <div 
                        className="dropleft d-flex justify-content-center h5" 
                        key={menuIdx}
                    >
                        <div 
                            className="pointer w-auto active font-roboto h5"
                            data-toggle="dropdown"
                            aria-expanded="false"
                            data-boundary="viewport"
                        >                                        
                            {menu.name}
                        </div>
                        <div className=" dropdown-menu disable-select mr-2 border border-white bg-black text-white w-auto">
                            <div className="row row-cols-2 pl-2 mb-2">
                                <FaRegEdit 
                                    className="pointer ml-auto"
                                    title="edit menu name"
                                    size={14}
                                    onClick={() => {
                                        setMenuNameToEdit(menuIdx); 
                                        setNewMenuName(menu.name);
                                    }}
                                    
                                    />
                                <RiDeleteBin7Line 
                                    className="pointer mr-auto"
                                    title="delete menu"
                                    size={16} 
                                    color={"white"}
                                    onClick={() => dispatch(deleteMenu(menuIdx))}
                                    />
                            </div>
                            <div className="pl-2 pr-2 pb-2">
                                <OldMenuTable oldMenuTable={menu.table} tableWidth={"col-12"} />
                            </div>
                        </div>
                    </div>
                )
            }
        </div>
     );
}
 
export default DatabaseOldMenusContent;