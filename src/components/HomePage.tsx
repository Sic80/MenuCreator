import React, { useState, useRef } from 'react';
import Navbar from './Navbar';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import useGetRangeOfDays from './useGetRangeOfDays';
import MenuTable from './MenuTable';
import { useDispatch, useSelector } from 'react-redux';
import { saveMenuToSend, toggleOldMenusPanels, toggleRecipesPanels, toggleShoppingListPanel } from './actions';
import ShoppingList from './ShoppingList';
import SlidingPanel from 'react-sliding-side-panel';
import 'react-sliding-side-panel/lib/index.css';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import RecipesDatabase from './RecipesDatabase';
import OldMenusDatabase from './OldMenusDatabase';
import '../css/Navbar.css';
import 'react-calendar/dist/Calendar.css';
import { ImArrowLeft, ImArrowRight } from 'react-icons/im'
import { ToastContainer, toast, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RootState } from './types/RootState';




const HomePage = () => {  
    
    const dispatch = useDispatch();
    const recipes = useSelector((state: RootState) => state.recipeDataBase.recipes);
    const [calendarSelection, setCalendarSelection] = useState<Date[]>([]);
    const getRangeOfDays = useGetRangeOfDays(calendarSelection[0], calendarSelection[1]);
    const [rangeOfDays, setRangeOfDays] = useState<string[]>([]);
    const scrollHere = useRef<HTMLDivElement>(null);
    const shoppingListPanel = useSelector((state: RootState) => state.slidingPanelsControl.shoppingListPanel)
    const recipesPanels = useSelector((state: RootState) => state.slidingPanelsControl.recipesPanels);
    const oldMenusPanels = useSelector((state: RootState) => state.slidingPanelsControl.oldMenusPanels);

    
    

    return(
        <div className=" bg-black text-center no-x-overflow">
            <div className="fixed-top mb-5">
                <Navbar/>
            </div>
            {recipes.length === 0 ?
                (<div className="col-12 d-flex justify-content-center bg-black text-white  offset">
                    <div className="text-center text-uppercase font-raleway">
                        <h1 className=" mt-2 big-text-responsive">before starting,</h1>
                        <h1 className="big-text-responsive">go to new recipe</h1>
                        <h1 className="big-text-responsive">and add your first recipe</h1>
                    </div>
                </div>)
                : 
                (<div> 
                    <div className="vh-100">
                        <div className="row row-cols-lg-2 row-cols-sm-1 mt-5 no-gutters">
                            <div className="col-lg-7 bg-black text-white pt-3 pt-sm-5 mt-2 mt-sm-5 d-flex justify-content-center text-uppercase font-raleway ">
                                <div className="text-left">
                                    <h1 className="mt-2 big-text-responsive ">Select the</h1>
                                    <h1 className="big-text-responsive">range of days</h1>
                                    <h1 className="big-text-responsive"> for your menu </h1>
                                </div>
                            </div>
                            <div className="col-lg-5 bg-black mt-3 mt-sm-5 pt-sm-5 pb-2 pb-sm-5 d-flex justify-content-center">
                                <Calendar
                                    className="react-calendar"
                                    selectRange={true} 
                                    defaultActiveStartDate={new Date()}
                                    returnValue="range"
                                    locale={"en"}
                                    onChange={(value: any) => setCalendarSelection(value)}//problems here
                                    minDate={new Date()}
                                />
                            </div>
                        </div>
                        <div className="row">                            
                            <div className="col text-center mt-5 mb-5">
                                <button 
                                    className="btn col-11 col-sm-10 col-md-3 bg-black text-white pt-sm-3 pb-sm-3 border-white"
                                    onClick={() => {
                                        if (calendarSelection.length > 0) {
                                            setRangeOfDays(getRangeOfDays);
                                            dispatch(saveMenuToSend({}));
                                            if(scrollHere.current) {scrollHere.current.scrollIntoView({block: "start"})};
                                        } else {
                                            toast.dark("Please, select a range of days for your menu")
                                        }
                                    }}
                                >
                                    <div className="h5 font-roboto pt-2">
                                        Create Menu Table
                                    </div>                            
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="row row-cols-2 row-cols-sm-2 row-cols-md-2 row-cols-lg-3 d-flex  justify-content-center mt-5">
                        
                        <div className="col-6 col-lg-2 mt-5">
                            <h5 
                                className="font-roboto pointer pl-5 pt-2 pb-2 pb-2 bg-white text-black text-right pr-2 mt-3 rounded-right sticky-top stick-to-nav"
                                onClick={() => dispatch(toggleRecipesPanels(0))}
                            >
                                Recipes 
                            </h5>
                        </div>
                        <div 
                            className="col-12 col-sm-12 col-md-10 col-lg-8 mt-5 order-12 order-lg-1"
                            ref={scrollHere}
                        >
                            <MenuTable
                                rangeOfDays={rangeOfDays}                            
                            />
                        </div>

                        <div className="col-sm-6 col-lg-2 order-lg-12 mt-5 margin-right-0">
                            <h5 
                                className="order-sm-last pl-2 pointer pt-2 rounded-left pb-2 text-left bg-white text-black mt-3 sticky-top stick-to-nav font-roboto"
                                onClick={() => dispatch(toggleOldMenusPanels(0))}
                            >
                                Old menus
                            </h5> 
                        </div>
                    </div>            
                </div>)
            }
                <SlidingPanel 
                    type={"left"}
                    isOpen={recipesPanels[0]}
                    noBackdrop={true}
                    panelContainerClassName=" bg-black col-12 col-sm-8 col-md-6 col-lg-4"
                    size={0}
                >
                    <div
                        className="bg-black h-100 overflow-hidden"
                    >
                        <div 
                            className="col text-white mt-5 mb-3 h5 pointer text-center border-bottom border-white pb-2"
                            onClick={() => dispatch(toggleRecipesPanels(0))}
                        > 
                            <ImArrowLeft size={25}/> 
                        </div>
                        <div>
                            <RecipesDatabase />
                        </div>
                    </div>
                </SlidingPanel>
                <SlidingPanel 
                    type={"right"}
                    isOpen={oldMenusPanels[0]}
                    noBackdrop={true}
                    size={25}
                    panelContainerClassName="bg-black col-12 col-sm-8 col-md-6 col-lg-4"
                >
                    <div
                        className="bg-black h-100 overflow-hidden"
                    >
                        <div 
                            className="col text-white  mt-5 mb-3 h5 pointer text-center border-bottom border-white pb-2"
                            onClick={() => dispatch(toggleOldMenusPanels(0))}
                        > 
                            <ImArrowRight size={25}/>
                        </div>
                        <div>
                            <OldMenusDatabase />
                        </div>
                    </div>                              
                </SlidingPanel>
                <SlidingPanel 
                    type={"bottom"}
                    isOpen={shoppingListPanel}
                    size={90}
                    panelContainerClassName="bg-white"

                >
                    <div
                        className="bg-white h-100"
                    >
                        <div className=" mt-4 pb-4 pt-4 bg-white item-align-right sticky-top"> 
                            <AiOutlineCloseCircle 
                                size={40} 
                                className="pointer"
                                onClick={() => dispatch(toggleShoppingListPanel())}
                            />
                        </div>
                        <div className="bg-white">
                            <ShoppingList />
                        </div>
                    </div>
                </SlidingPanel>
                <ToastContainer 
                    position="bottom-center"
                    autoClose={4000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    limit={1}
                    transition={Slide}            
                />
        </div>
         
    );
}
                
export default HomePage;

