import React from 'react';
import { useDispatch } from 'react-redux';
import { toggleEditRecipePanel, deleteRecipe } from './actions';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin7Line } from 'react-icons/ri';
import { Recipe } from './types/RecipeInterface';

interface DatabaseRecipeContentProps {
    recipe: Recipe,
    recipeIdx: number,
    setRecipeToEditIdxFromChild: Function
}

const DatabaseRecipeContent = ({recipe, recipeIdx, setRecipeToEditIdxFromChild}: DatabaseRecipeContentProps) => {

    const dispatch = useDispatch();

    return ( 
        <div 
            className="dropright mt-1 mb-1 d-flex justify-content-center"
        >
            <div 
                className="pointer active font-roboto h5  " 
                data-toggle="dropdown"
                data-boundary="viewport"
                aria-expanded="false"
            >
                <div>{recipe.recipeName}</div>                                   
            </div>
            <div className="dropdown-menu pl-2  pr-2 ml-2 border border-white bg-black text-white sticky-top w-auto" >
                <div className="row row-cols-2 pr-2 mt-2 mb-2">
                    <FaRegEdit 
                        className="pointer ml-auto"
                        title="edit recipe"
                        size={12}
                        onClick={() => {
                            dispatch(toggleEditRecipePanel());
                            setRecipeToEditIdxFromChild(recipeIdx);
                        }}
                        
                    />
                    <RiDeleteBin7Line 
                        className="pointer mr-auto"
                        title="delete recipe"
                        size={16} 
                        color={"white"}
                        onClick={() => dispatch(deleteRecipe(recipeIdx))}
                    />
                </div>
                <div className="h6 text-center">
                    {recipe.recipeName}
                </div>
                {recipe.ingredients.map((ingredient, ingredientIdx) =>
                    <div key={ingredientIdx} className="disable-select">
                        {ingredient.name} {ingredient.quantity !== "0" ? ingredient.quantity : ""} {ingredient.unit}
                    </div>
                )}
            </div>
        </div>
    );
}
 
export default DatabaseRecipeContent;