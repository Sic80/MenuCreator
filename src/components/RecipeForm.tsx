import React, { useState, useRef, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { saveRecipe, deleteRecipe, toggleEditRecipePanel } from './actions';
import { RiDeleteBin7Line } from 'react-icons/ri';
import { ToastContainer, toast, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RootState } from './types/RootState';
import { Recipe } from './types/RecipeInterface'

interface RecipeFormProps {
    initialState: Recipe,
    isToEdit: [boolean, number]
}


const RecipeForm = ({ initialState, isToEdit }: RecipeFormProps) => {

    const _ = require('lodash');
    const dispatch = useDispatch();
    const recipes = useSelector((state: RootState) => state.recipeDataBase.recipes);
    
    const ingredientEmptyInputs =  {name: "", quantity: "", unit: ""};
    const emptyForm = {recipeName: "", ingredients: [{...ingredientEmptyInputs}]}

    const [inputsValues, setInputsValues] = useState(initialState);
    const selectOptions = ["Kg", "gr", "L", "mL", "tbsp", "tsp", "pcs", "TT", "glass"];

    const isRecipeNameEmpty: boolean = (inputsValues.recipeName === "");
    const isRecipeNameUsed: boolean = (recipes.some(recipe => inputsValues.recipeName.replaceAll(" ", "") === recipe.recipeName.replaceAll(" ", ""))) && !isToEdit[0];
    const areIngredientsFieldsEmpty: boolean = inputsValues.ingredients.some(ingredient => Object.values(ingredient).includes(""));

    const inputToFocus = useRef<HTMLInputElement>(null);
    const recipeNameToFocus = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if(recipeNameToFocus.current) {recipeNameToFocus.current.focus()};        
    }, []);
    
    useEffect(() => {
        if (inputToFocus.current && inputsValues.ingredients.length > 1 && inputsValues.ingredients.some(ingredient => Object.values(ingredient).includes(""))) {
            inputToFocus.current.focus();        
        }
    }, [inputsValues.ingredients]);

    const changeIngredientValue = (value: string, idx: number, inputName: string) => {
        const inputsValuesCopy = {...inputsValues}
        inputsValuesCopy.ingredients[idx][inputName] = value;

        if ((inputName === "unit" && value === "TT" && (inputsValues.ingredients[idx].quantity !== "0")) || (inputName === "quantity" && inputsValues.ingredients[idx].unit === "TT")) {
            inputsValuesCopy.ingredients[idx].quantity = "0";
            toast.dark(accordMessageToError(false, true))
        }
        setInputsValues(inputsValuesCopy);
    }

    const accordMessageToError = (success?: boolean, TT?: boolean) => {
        let errorMessage = "";
        if (isRecipeNameUsed) {
            errorMessage = "The recipe name is already in use";
        } else if (TT) {
            errorMessage = "Quantity has to be 0 if unit is 'TT'"
        } else if (isRecipeNameEmpty && !areIngredientsFieldsEmpty) {
            errorMessage = "Please, choose a recipe name"
        } else if (isRecipeNameEmpty || areIngredientsFieldsEmpty) {
            errorMessage = "Please, fill all the fields"
        } else if (success) {
            errorMessage = "Saved";
        };
        return errorMessage;
    }

    return(
        <div className="mt-5 bg-black no-x-overflow">
            <div className="form-group text-center mb-sm-5">
                <input 
                    className="col-8" 
                    autoComplete="off"
                    spellCheck="false"
                    ref={recipeNameToFocus}
                    name="recipeName" 
                    placeholder="Recipe Name" 
                    type="text" 
                    value={_.capitalize(inputsValues.recipeName).replace(/^\s/, "").replace(/ {1,}/g, " ")}
                    onChange={(e) => {
                        const inputsValuesCopy = {...inputsValues};
                        inputsValuesCopy.recipeName = e.target.value;
                        setInputsValues(inputsValuesCopy);
                    }}
                />
            </div>
            {inputsValues.ingredients.map((ingredient, idx: number) =>
                <div 
                    className="row row-cols-1 row-sm-cols-4 form-group text-center justify-content-center mb-2"
                    key={idx}
                >
                    <input
                        className="col-4 col-sm-6 pl-1 pr-0"
                        autoComplete="off"
                        spellCheck="false"
                        ref={inputToFocus}
                        name="name" 
                        placeholder="Ingredient name" 
                        type="text" 
                        value={ingredient.name.toLowerCase()}
                        onChange={(e) => changeIngredientValue(e.target.value, idx, e.target.name)}
                    />
                    <input
                        className="col-2 col-sm-2" 
                        autoComplete="off"
                        spellCheck="false"
                        name="quantity" 
                        placeholder="qty" 
                        type="text" 
                        value={ingredient.quantity.replace(",",".")}
                        onChange={(e) => changeIngredientValue(e.target.value, idx, e.target.name)}
                    />
                    <select  
                        className="col-3 col-sm-2"                                                               
                        name="unit"
                        value={ingredient.unit}
                        onChange={(e) => changeIngredientValue(e.target.value, idx, e.target.name)}
                    >
                        <option value="" disabled>unit</option>
                        {selectOptions.map((option, optIdx) =>
                            <option key={optIdx} value={option}>{option}</option> 
                        )}
                    </select>                                   
                    <div className="col-1 text-white ml-1">
                        {inputsValues.ingredients.length > 1 &&
                            <RiDeleteBin7Line 
                                className="pointer ml-n1 ml-sm-1"
                                title="delete ingredient"
                                onClick={() => {
                                    const { ingredients } = inputsValues;
                                    const ingredientsCopy = [...ingredients.slice(0, idx), ...ingredients.slice(idx+1, ingredients.length)];
                                    setInputsValues({recipeName: inputsValues.recipeName, ingredients: ingredientsCopy});
                                }}
                        />}
                    </div>
                </div>
            )}             
            <div className="row row-lg-2 d-flex justify-content-center pb-3 mt-5 pt-4">
                <button
                    className="col-10 col-md-4 btn bg-black border-white text-white ml-1 mb-5 pt-4 pb-4 pt-sm-2 pb-sm-2"
                    onClick={() => {
                        if (isRecipeNameEmpty){
                            toast.dark(accordMessageToError());
                        } else if (areIngredientsFieldsEmpty) {
                            toast.dark(accordMessageToError());
                        } else {
                            const ingredientsCopy = [...inputsValues.ingredients, {...ingredientEmptyInputs}];
                            setInputsValues({recipeName: inputsValues.recipeName, ingredients: ingredientsCopy});
                        } 
                    }}
                >
                    Add ingredient              
                </button>     
                <button 
                    className="col-10 col-md-4 btn bg-black text-white border-white ml-1 mb-5 pt-4 pb-4 pt-sm-2 pb-sm-2"
                    onClick={() => {
                        if (!(isRecipeNameEmpty || areIngredientsFieldsEmpty || isRecipeNameUsed)) {
                            if (isToEdit[0]) {
                                dispatch(deleteRecipe(isToEdit[1]));
                                dispatch(toggleEditRecipePanel());
                            }
                            dispatch(saveRecipe(inputsValues));
                            setInputsValues({...emptyForm});
                            toast.dark(accordMessageToError(true))
                        } else {
                            toast.dark(accordMessageToError())
                        }
                    }}
                    >
                        Save
                </button>
            </div>
            <ToastContainer 
                position="bottom-center"
                autoClose={2500}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                limit={1}
                transition={Slide} 
                bodyClassName={"text-center w-100"}                          
            />
        </div>
    );             
}

export default RecipeForm;